public class Circle{

    int r;
    Point center;

    public Circle(Point p, int r){
        this.r = r;
        this.center = p;
    }

    public double area(){
        //Calculates area of circle
        return Math.PI * r * r;
    }

    public double perimeter(){
        //Calculates perimeter of circle
        return 2 * Math.PI * r;
    }

    public boolean intersect(Circle c){
        //Controlls circles if they are intersects return true else false

        double distance1 = Math.pow((this.center.xCoord - c.center.xCoord),2) + Math.pow((this.center.yCoord - c.center.yCoord),2);
        double distance = Math.sqrt(distance1);

        if(this.r+c.r > distance)
            return true;
        else
            return false;

    }
}

