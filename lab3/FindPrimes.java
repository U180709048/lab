public class FindPrimes {
    public static void main(String[] args){
        int value = Integer.parseInt(args[0]);
        String primeString = "";
        
        for (int i = 2; i <= value; i++) {
            if(isPrime(i)){
                primeString = primeString+i+",";
              }
        
            }
        int lenString = primeString.length();
        primeString = primeString.substring(0,lenString-1);
        System.out.print(primeString);
}

    public static boolean isPrime(int i){

        if ( i == 2)
            return true;
        else{
            for(int c = 2; c<i; c++){
               if (i % c == 0)
                    return false;
            }
            return true;
        }
    }
}